import asyncio
import aiohttp
from time import time

async def request_get(session, url, queue):
    async with session.get(url) as response:
        print(f"Entering coroutine with {url}")
        await queue.put(await response.json())
    print(f"Exiting coroutine with {url}")

async def main():
    queue = asyncio.Queue()
    tasks = []
    url_list = [
        'https://swapi.co/api/people',
        'https://swapi.co/api/people/?page=2',
        'https://swapi.co/api/people/?page=3',
        'https://swapi.co/api/people/?page=4',
    ]
    start = time()
    async with aiohttp.ClientSession() as session:
        for url in url_list:
            task = asyncio.create_task(request_get(session, url, queue))
            tasks.append(task)
        await asyncio.gather(*tasks)
    print(f'Time taken: {time() - start}')

asyncio.run(main())