import asyncio


async def compute(x, y, data):
    print(f"Computing the value of {x} and {y}")
    result = x + y
    await data.put(result)


async def process(n, data):
    processed, sumx = 0, 0
    while processed < n:
        item = await data.get()
        print(item)
        processed += 1
        value = item
        sumx += value
    print(f"The sum is:{sumx}")
    await asyncio.sleep(.5)


def main():
    loop = asyncio.get_event_loop()
    data = asyncio.Queue()
    sum1 = loop.create_task(compute(1, 4, data))
    sum2 = loop.create_task(compute(0, 0, data))
    sum3 = loop.create_task(process(2, data))
    final_task = asyncio.gather(sum1, sum2, sum3)
    print("This is:", final_task)
    loop.run_until_complete(final_task)


if __name__ == '__main__':
    main()
