import json
from aiohttp import web
import asyncio

routes = web.RouteTableDef()

global db


@routes.post('/json')
async def json_greeter(request):
    data = await request.post()
    username = data.get('username')
    json_data = {'name': username}
    return web.json_response(json_data)


@routes.get('/user')
async def greet_user(request: web.Request) -> web.Response:
    users =  db
    json_data = {'name': users}
    return web.json_response(json_data)


async def init_app() -> web.Application:
    app = web.Application()
    app.add_routes(routes)
    return app

def fake_db():
    with open('db.json') as json_data:
        global db
        db = json.load(json_data)

fake_db()

web.run_app(init_app())
