import asyncio


async def compute(x, y):
    print(f"Computing the value of {x} and {y}")
    await asyncio.sleep(1)
    return x + y


async def print_sum():
    value1 = asyncio.create_task(compute(1, 0))
    value2 = asyncio.create_task(compute(1, 0))
    value3 = asyncio.create_task(compute(1, 0))
    print(sum(await asyncio.gather(value1, value2, value3)))

asyncio.run(print_sum())
