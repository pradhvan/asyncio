import threading
import queue
import requests
from time import time

def request_thread(url, data_queue):
    print(f"Entered thread with: {url}")
    request = requests.get(url)
    data_queue.put(request.json())
    print(f"Exiting thread with: {url}")

def main():
    urls = [
        'https://swapi.co/api/people',
        'https://swapi.co/api/people/?page=2',
        'https://swapi.co/api/people/?page=3',
        'https://swapi.co/api/people/?page=4'
    ]
    data_queue = queue.Queue()
    threads = []
    start = time()
    for url in urls:
        threads.append(threading.Thread(target=request_thread, args=(url,data_queue)))
    for thread in threads:
        thread.start()
    for thread in threads:
        thread.join()

    print(f'Time taken: {time() - start}')


if __name__ == '__main__':
    main()