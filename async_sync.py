import asyncio
import concurrent.futures


def compute(x, y):
    print(f"Computing the value of {x} and {y}")
    return x + y


async def print_sum():
    executor = concurrent.futures.ThreadPoolExecutor(max_workers=3)
    loop = asyncio.get_event_loop()
    value1 = await loop.run_in_executor(executor, compute, 1, 2)
    value2 = await loop.run_in_executor(executor, compute, 1, 2)
    print(value1 + value2)


asyncio.run(print_sum())
