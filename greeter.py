import asyncio


async def greeter(name):
    """Coroutine"""
    await asyncio.sleep(1)
    print(f'Hello {name}')


async def main():
    # create_task: Wraps the coroutine into a task and schedules it's execution
    task1 = asyncio.create_task(greeter('Guido'))
    task2 = asyncio.create_task(greeter('Luciano'))
    task3 = asyncio.create_task(greeter('Kushal'))
    # For concurrently running multiple tasks.
    asyncio.gather(task1, task2, task3)

asyncio.run(main())
