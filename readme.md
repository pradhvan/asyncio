# Asyncio

Slides for this  can be found [here]( https://docs.google.com/presentation/d/1g5PxnKaPJVp23QiTBC6f4R5rRsrJyO1wA9CkrDlKQ4g/edit?usp=sharing).

# Refrence

The blog post [Talking Concurrency: Asyncio](https://blogs.dgplug.org/pradhvan/talking-concurrency-2) covers most of ground talked in the slides and can be a good starting point if you're intrested in the whole paradigm of Async programming.

# Things to check out next: 

[Advanced asyncio: Solving Real-world Production Problems](https://www.youtube.com/watch?v=bckD_GK80oY) is a followup to the whole series, you can check that out to see the real world problems solved by Asyncio and want to know the pitfalls you should avoid while
writing concurrent code. 

[Asyncio in Python 3 7 and 3 8](https://www.youtube.com/watch?v=ReXxO_azV-w) yurv selivanov talk from EuroPython covers the whole design principles of Asyncio from Python 3.4 to the new beta released Python 3.8.
