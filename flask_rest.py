from flask import Flask
from flask import json


app = Flask(__name__)

global db


def fake_db():
    with open('db.json') as json_data:
        global db
        db = json.load(json_data)


@app.route('/user')
def hello_world():
    response = json.dumps(db)
    return response


if __name__ == '__main__':
    fake_db()
    app.run(debug=True)
